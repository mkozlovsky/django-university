from django.contrib.auth import get_user
from django.contrib.auth.decorators import login_required
from django.shortcuts import render

from lessons.models import LessonResult


@login_required
def student_profile(request):
    user = get_user(request)
    results = LessonResult.objects.filter(user=user)
    context = {
        "student": user,
        "lesson_results": results
    }
    return render(request, "profiles/student_profile.html", context)
