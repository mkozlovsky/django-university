from django.contrib import admin

# Register your models here.
from lessons.models import Course, Lesson, LessonResult

admin.site.register(Course)
admin.site.register(Lesson)
admin.site.register(LessonResult)