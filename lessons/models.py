from django.contrib.auth.models import User
from django.core.validators import MinValueValidator, MaxValueValidator
from django.db import models


class Course(models.Model):
    """
    This class represents course to learn
    """
    title = models.CharField(max_length=252, null=False)
    description = models.TextField(null=True)
    created_at = models.DateField(auto_now=True)

    def __str__(self):
        return self.title


class Lesson(models.Model):
    title = models.CharField(max_length=252, null=False)
    description = models.TextField(null=False)
    course = models.ForeignKey(Course, on_delete=models.CASCADE)

    def __str__(self):
        return self.title
    

class LessonResult(models.Model):
    """
    Class that links user with lesson he/she working on
    """
    lesson = models.ForeignKey(Lesson, on_delete=models.SET_NULL, null=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    answer = models.TextField(null=False)
    sent_at = models.DateTimeField(auto_now=True)
    teacher_rate = models.PositiveIntegerField(default=0, validators=[MinValueValidator(1), MaxValueValidator(12)])
    revisited_at = models.DateTimeField(null=True)

