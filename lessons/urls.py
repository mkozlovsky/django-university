from django.urls import path
from . import views

urlpatterns = [
    path('', views.list_courses, name="list_courses"),
    path('<int:course_id>', views.view_course, name="view_course"),
    path('lesson/<int:lesson_id>', views.view_lesson, name="view_lesson")
]