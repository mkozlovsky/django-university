from django.contrib.auth import get_user
from django.contrib.auth.decorators import login_required
from django import forms
from django.http import HttpResponse
from django.shortcuts import render, redirect
from lessons.models import Course, Lesson, LessonResult


@login_required
def list_courses(request):
    """
    list of courses available for student
    :param request:
    :return:
    """
    courses = Course.objects.order_by('-created_at')[:5]
    context = {"courses": courses}
    return render(request, "lessons/list.html", context)


@login_required
def view_course(request, course_id):
    course = Course.objects.get(pk=course_id)
    lessons = Lesson.objects.filter(course=course_id)
    context = {"lessons": lessons[:5], "course": course}
    return render(request, "lessons/list_lessons.html", context)


class LessonAnswerForm(forms.Form):
    answer = forms.CharField(min_length=120)


@login_required
def view_lesson(request, lesson_id):
    lesson = Lesson.objects.get(pk=lesson_id)
    if request.method == 'POST':
        form = LessonAnswerForm(request.POST)
        if form.is_valid():
            lesson_result = LessonResult()
            lesson_result.answer = form.cleaned_data['answer']
            lesson_result.lesson = lesson
            lesson_result.user = get_user(request)
            lesson_result.save()
            return redirect('view_course', lesson.course.id)
        else:
            return HttpResponse(form.errors)
    context = {"lesson": lesson}
    return render(request, "lessons/view_lesson.html", context)




